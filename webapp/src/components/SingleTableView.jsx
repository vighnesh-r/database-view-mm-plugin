import React, { useState, useEffect } from "react";

import { Client4 } from "mattermost-redux/client";

import { id as pluginId } from "../manifest";

const b64Decode = (value) => {
    if (value === "AA==") {
        return "false";
    }
    if (value === "AQ==") {
        return "true";
    }
    return atob(value);
};

function SingleTableView(props) {
    const tableName = props.tableName;
    const [tableData, setTableData] = useState([]);

    const fetchData = () => {
        const getTableInfo = `/plugins/${pluginId}/get/specific-table`;
        fetch(
            getTableInfo,
            Client4.getOptions({
                method: "POST",
                body: tableName,
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                },
            })
        )
            .then((res) => res.json())
            .then((tableInfo) => {
                console.log(tableInfo);
                setTableData(tableInfo);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        fetchData();
    }, [tableName]);

    return (
        <div>
            <button onClick={props.unset}>Back</button>
            <button onClick={fetchData}>Refresh</button>
            <h3>{tableName}</h3>
            {tableData.length === 0 && <h5>Table is empty</h5>}
            {tableData.length > 0 && (
                <table className="table table-striped table-bordered table-hover">
                    <thead className="thead-dark">
                        <tr>
                            {Object.keys(tableData[0]).map((columnName) => (
                                <th key={columnName}>{columnName}</th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {tableData.map((row, index) => {
                            const columnNames = Object.keys(row);
                            return (
                                <tr key={index}>
                                    {columnNames.map((columnName) => {
                                        const b64EncodedValue =
                                            row[columnName] === null
                                                ? "bnVsbA==" // b64 of 'null'
                                                : row[columnName];
                                        const value = b64Decode(
                                            b64EncodedValue
                                        );
                                        return (
                                            <td key={columnName}>{value}</td>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
        </div>
    );
}

export default SingleTableView;
