import React, { useState, useEffect } from "react";

import { id as pluginId } from "../manifest";
import SingleTableView from "./SingleTableView";

function AllTablesInDB() {
    const [tables, setTables] = useState([]);

    const [selectedTable, setSelectedTable] = useState("");

    useEffect(() => {
        fetch(`/plugins/${pluginId}/get/tables`)
            .then((res) => res.json())
            .then((tableNames) => {
                setTables(tableNames);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const onTableSelect = (tableName) => {
        console.log(tableName + " selected.");
        setSelectedTable(tableName);
    };

    return (
        <div>
            {selectedTable.length === 0 && (
                <>
                    <h2>All Tables in the Database</h2>
                    <ol>
                        {tables.map((table) => (
                            <li key={table}>
                                <a onClick={() => onTableSelect(table)}>
                                    {table}
                                </a>
                            </li>
                        ))}
                    </ol>
                </>
            )}

            {selectedTable.length > 0 && (
                <SingleTableView
                    tableName={selectedTable}
                    unset={() => setSelectedTable("")}
                />
            )}
        </div>
    );
}

export default AllTablesInDB;
