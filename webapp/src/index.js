import manifest from './manifest';

import AllTablesInDB from './components/AllTablesInDB';

export default class Plugin {
    initialize(registry, store) {
        registry.registerAdminConsoleCustomSetting("DatabaseView", AllTablesInDB);
    }
}

window.registerPlugin(manifest.id, new Plugin());
