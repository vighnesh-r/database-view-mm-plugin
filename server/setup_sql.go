package main

import (
	"database/sql"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func getDBConnection() (*sql.DB, error) {
	rawConnectionString := os.Getenv("MM_CONFIG")
	connectionString := ""
	if len(rawConnectionString) > 0 {
		connectionString = os.Getenv("MM_CONFIG")[8:]
	} else {
		connectionString = "mmuser:mostest@(0.0.0.0:3306)/mattermost_test"
	}
	db, err := sql.Open("mysql", connectionString)
	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(100)

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}
