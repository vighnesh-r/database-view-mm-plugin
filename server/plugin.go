package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	"github.com/mattermost/mattermost-server/v5/plugin"

	_ "github.com/go-sql-driver/mysql"
)

// Plugin implements the interface expected by the Mattermost server to communicate between the server and plugin processes.
type Plugin struct {
	plugin.MattermostPlugin

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration
}

// DB handle
var mysqlDB *sql.DB

// ServeHTTP demonstrates a plugin that handles HTTP requests by greeting the world.
func (p *Plugin) ServeHTTP(c *plugin.Context, w http.ResponseWriter, r *http.Request) {
	userID := r.Header.Get("Mattermost-User-ID")
	if userID == "" {
		http.Error(w, "Not authorized. Missing MMUID.", http.StatusUnauthorized)
		return
	}

	switch path := r.URL.Path; path {
	case "/get/tables":
		p.GetAllTables(w)
		return
	case "/get/specific-table":
		p.GetSingleTableInfo(w, r)
		return
	default:
		fmt.Fprint(w, "Default Route triggerred")
	}
}

// OnActivate is invoked when a plugin is activated
func (p *Plugin) OnActivate() error {
	var err error
	mysqlDB, err = getDBConnection()
	return err
}

// GetAllTables returns all the table names
func (p *Plugin) GetAllTables(w http.ResponseWriter) {
	rows, err := mysqlDB.Query("show tables;")
	if err != nil {
		fmt.Fprint(w, "Failed to fetch all tables. "+err.Error())
		return
	}

	tableNames := []string{}
	for rows.Next() {
		var TableName string
		tableNameScanErr := rows.Scan(&TableName)
		if tableNameScanErr != nil {
			fmt.Fprint(w, "Failed to fetch a table name. "+tableNameScanErr.Error())
			return
		}
		tableNames = append(tableNames, TableName)
	}

	responseBody, marshalErr := json.Marshal(tableNames)
	if marshalErr != nil {
		fmt.Fprint(w, "Failed to marshal the data. "+marshalErr.Error())
		return
	}

	fmt.Fprint(w, string(responseBody))
}

// GetSingleTableInfo returns all the data in the table.
func (p *Plugin) GetSingleTableInfo(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Couldn't read body.", http.StatusBadRequest)
		return
	}

	tableName := string(body)

	sqlcommand := fmt.Sprintf("select * from %s;", tableName)
	rows, err := mysqlDB.Query(sqlcommand)
	if err != nil {
		fmt.Fprint(w, "Failed to 'select *'. Table -> "+tableName+". "+err.Error())
		return
	}
	cols, err := rows.Columns()
	if err != nil {
		fmt.Fprint(w, "Failed to fetch column names. Table -> "+tableName+". "+err.Error())
		return
	}

	responseRows := []map[string]interface{}{}
	for rows.Next() {
		rowData := make(map[string]interface{})
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}

		err = rows.Scan(columnPointers...)
		if err != nil {
			fmt.Fprint(w, "Failed to scan specific row. Table -> "+tableName+". "+err.Error())
			return
		}

		for i, colName := range cols {
			rowData[colName] = columns[i]
		}
		responseRows = append(responseRows, rowData)
	}

	responseBody, marshalErr := json.Marshal(responseRows)
	if marshalErr != nil {
		fmt.Fprint(w, "Failed to marshal the data. "+marshalErr.Error())
		return
	}

	fmt.Fprint(w, string(responseBody))
}
